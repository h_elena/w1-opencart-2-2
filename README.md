=== Wallet One Checkout ===
Contributors: Wallet One Единая касса / Wallet One Checkout
Version: 3.0.0
Tags: Wallet One, Opencart, buy now, payment, payment for Opencart
Requires at least: 2.2.0.0
Tested up to: 2.2.0.0
Stable tag: 2.2.0.0
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One Checkout module is a payment system for Opencart. He it allows to pay for your orders on the site.

== Description ==
If you have an online store on Opencart, then you need a module payments for orders made. This will help you plug the payment system Wallet One Checkout. With our system you will be able to significantly expand the method of receiving payment. This will lead to an increase in the number of customers to your store.

The latest version of the module you can be found at the link https://bitbucket.org/h_elena/w1-opencart/downloads

== Installation ==
1. Register on the site https://www.walletone.com/ru/merchant/
2. Download the module files on the repository and install.
3. Activate the module.
4. Instructions for configuring the module is in site https://www.walletone.com/ru/merchant/modules/opencart-cms/ .

== Screenshots ==

== Changelog ==
= 1.0.1 =
* Fix - fixed bag for payment system, 27/05/2016

= 1.0.2 =
* Fix - fixed bag for select currency and version php, 07/06/2016

= 1.0.3 =
* Add - added new fieds in settings, 21/06/2016

= 2.0.0 =
* Changed a module for osStore, 07/10/2016

= 2.1.1 =
* Fixed bug with language definition and add new settings - culture

= 2.1.2 =
* Fixed bug with language culture id

= 3.0.0 =
* Fixed bug with anser from calback payment system

== Frequently Asked Questions ==
No recent asked questions 

== Upgrade Notice ==
No recent asked updates

